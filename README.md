<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">地平线 EDA</font></font></h1><a id="user-content-horizon-eda" class="anchor" aria-label="永久链接：Horizo&ZeroWidthSpace;&ZeroWidthSpace;n EDA" href="#horizon-eda"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Horizo&ZeroWidthSpace;&ZeroWidthSpace;n EDA 是一个</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">电子设计自动化</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">软件包，支持印刷电路板设计的集成端到端工作流程，包括零件管理和原理图输入。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请参阅</font></font><a href="https://docs.horizon-eda.org/en/latest/feature-overview.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以了解 Horizo&ZeroWidthSpace;&ZeroWidthSpace;n 的主要功能概述。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">想聊聊这个项目吗？加入 libera.chat 上的 #horizo&ZeroWidthSpace;&ZeroWidthSpace;n-eda</font></font></p>
<p dir="auto"><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/455e86441326d86175d1ec24e435496efbf34925437c62244b349f8ebaeb3ae0/68747470733a2f2f686f72697a6f6e2d6564612e6f72672f73637265656e73686f74732f636f6c6c6167652e706e67"><img src="https://camo.githubusercontent.com/455e86441326d86175d1ec24e435496efbf34925437c62244b349f8ebaeb3ae0/68747470733a2f2f686f72697a6f6e2d6564612e6f72672f73637265656e73686f74732f636f6c6c6167652e706e67" alt="拼贴画" data-canonical-src="https://horizon-eda.org/screenshots/collage.png" style="max-width: 100%;"></a></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">目前为用户提供的功能</font></font></h1><a id="user-content-features-for-users-so-far" class="anchor" aria-label="永久链接：迄今为止为用户提供的功能" href="#features-for-users-so-far"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从原理图输入到 Gerber 导出的完整设计流程</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">理智的图书馆管理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从符号到电路板的所有内容均采用统一的编辑器</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">网络表感知原理图编辑器</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">KiCad 出色的交互式路由器</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">无延迟和无故障的渲染</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基于规则的 DRC</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">层次结构示意图</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">撤销重做</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">复制/粘贴某些对象</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Linux 和 Windows 上构建并运行</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">面向开发人员的功能</font></font></h1><a id="user-content-features-for-developers" class="anchor" aria-label="永久链接：面向开发人员的功能" href="#features-for-developers"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用现代 C++ 编写，无遗留代码库！</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 JSON 作为磁盘格式</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Gtkmm3 作为 GUI</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenGL 3 加速渲染</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">一切都通过 UUID 引用</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">入门</font></font></h1><a id="user-content-getting-started" class="anchor" aria-label="永久链接：入门" href="#getting-started"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">参见</font></font><a href="https://docs.horizon-eda.org/en/latest/installation.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">包含第三方软件</font></font></h1><a id="user-content-included-third-party-software" class="anchor" aria-label="永久链接：包含第三方软件" href="#included-third-party-software"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">目录</font></font><code>3rd_party</code></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">项目</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">版本</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">网址</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">执照</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">恩洛曼</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">现代 C++ 的 JSON</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3.10.3</font></font></td>
<td><a href="https://github.com/nlohmann/json/"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/nlohmann/json/</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">麻省理工学院</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">剪刀</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">快船</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">6.4.2</font></font></td>
<td><a href="http://www.angusj.com/delphi/clipper.php" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.angusj.com/delphi/clipper.php</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">促进</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多分区</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多分区</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">7bdffb428b2b19ad1c43aa44c714dcc104177e84</font></font></td>
<td><a href="https://github.com/ivanfratric/polypartition/"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/ivanfratric/polypartition/</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">麻省理工学院</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">聚2三</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">聚2三</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">d949f3cd6f85b20728af0bdc454b090226068c73</font></font></td>
<td><a href="https://github.com/jhasse/poly2tri"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/jhasse/poly2tri</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3 条款 BSD</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">库</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">库</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3.26.4</font></font></td>
<td><a href="https://qcad.org/en/90-dxflib" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://qcad.org/en/90-dxflib</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPLv2</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阿尔法</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Alphanum 算法</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1.3</font></font></td>
<td><a href="http://www.davekoelle.com/alphanum.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.davekoelle.com/alphanum.html</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">麻省理工学院</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">德劳纳特</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">德劳纳特 C++</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">6f2879967bc96a9bcdbacf418e560e9f2e170ace</font></font></td>
<td><a href="https://github.com/abellgithub/delaunator-cpp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/abellgithub/delaunator-cpp</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">麻省理工学院</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">脚标</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">脚标</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">99116328abe8f53e71831b446d35e93ee7128ef3</font></font></td>
<td><a href="https://github.com/endofexclusive/footag"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/endofexclusive/footag</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPLv3</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">路由器</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">KiCad 路由器</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">6.0.4</font></font></td>
<td><a href="https://gitlab.com/kicad/code/kicad/-/tree/6.0.4/pcbnew/router" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://gitlab.com/kicad/code/kicad/-/tree/6.0.4/pcbnew/router</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPLv3</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">sexpr</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">KiCad s-表达式解析器</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">6.0.4</font></font></td>
<td><a href="https://gitlab.com/kicad/code/kicad/-/tree/6.0.4/libs/sexpr" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://gitlab.com/kicad/code/kicad/-/tree/6.0.4/libs/sexpr</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPLv3</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">捕获2</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">抓住2</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3.0.1</font></font></td>
<td><a href="https://github.com/catchorg/Catch2/releases/tag/v3.0.1"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/catchorg/Catch2/releases/tag/v3.0.1</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">促进</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">范围-v3</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">范围-v3</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">0.12.0</font></font></td>
<td><a href="https://github.com/ericniebler/range-v3/releases/tag/0.12.0"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/ericniebler/range-v3/releases/tag/0.12.0</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">促进</font></font></td>
</tr>
</tbody>
</table>
<ul dir="auto">
<li><a href="https://github.com/russdill/pybis"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/russdill/pybis</font></font></a></li>
</ul>
</article></div>
